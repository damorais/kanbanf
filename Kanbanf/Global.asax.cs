﻿using Kanbanf.Models.Entidades;
using Kanbanf.Models.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Kanbanf
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            IQuadrosRepository repo = new QuadrosEmMemoriaRepository();
            repo.Incluir("Minhas Tarefas");
            repo.Incluir("Projeto de LP3");
            repo.Incluir("Festa");
        }
    }
}
