﻿using Kanbanf.Models.Entidades;
using Kanbanf.Models.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kanbanf.Models.Servicos
{
    public class GruposService
    {
        public void Incluir(string nome, int idQuadro)
        {
            var quadro = new QuadrosEmMemoriaRepository().Obter(idQuadro);

            if (quadro != null)
            {
                var gruposRepository = new GruposEmMemoriaRepository();
                var ultimaPosicao = gruposRepository.ObterMaximaPosicaoGrupo(idQuadro);

                gruposRepository.Incluir(nome, ultimaPosicao + 1, quadro);
            }
            else
            {
                throw new ArgumentException("O quadro especificado não foi encontrado");
            }
        }

        public IEnumerable<Grupo> ObterAtivos(int idQuadro)
        {
            var gruposRepository = new GruposEmMemoriaRepository();
            return gruposRepository.ObterGrupos(idQuadro);
        }

        public Grupo Obter(int id)
        {
            var gruposRepository = new GruposEmMemoriaRepository();
            return gruposRepository.Obter(id);
        }

        public Quadro ObterQuadro(int idQuadro)
        {
            var quadro = new QuadrosEmMemoriaRepository().Obter(idQuadro);
            return quadro;
        }

        public void Remover(int id)
        {
            var gruposRepository = new GruposEmMemoriaRepository();
            gruposRepository.Remover(id);
        }

        public void Alterar(int id, string novoNome)
        {
            var gruposRepository = new GruposEmMemoriaRepository();
            gruposRepository.Atualizar(id, novoNome);
        }

        public void MoverParaADireita(int idQuadro, int id)
        {
            var gruposRepository = new GruposEmMemoriaRepository();
            var grupo = gruposRepository.Obter(id);
            if (grupo.Posicao > 1)
            {
                var grupoADireita = gruposRepository.ObterGrupoMenorPosicao(grupo.Posicao);
                this.SwapGrupos(grupo, grupoADireita);
            }
        }

        public void MoverParaAEsquerda(int idQuadro, int id)
        {
            var gruposRepository = new GruposEmMemoriaRepository();
            var grupo = gruposRepository.Obter(id);
            if (grupo.Posicao < gruposRepository.ObterMaximaPosicaoGrupo(idQuadro))
            {
                var grupoAEsquerda = gruposRepository.ObterGrupoMaiorPosicao(grupo.Posicao);

                this.SwapGrupos(grupo, grupoAEsquerda);
            }
        }

        private void SwapGrupos(Grupo grupoA, Grupo grupoB)
        {
            var gruposRepository = new GruposEmMemoriaRepository();

            var posicaoA = grupoA.Posicao;
            var posicaoB = grupoB.Posicao;

            gruposRepository.AtualizarPosicao(grupoA, posicaoB);
            gruposRepository.AtualizarPosicao(grupoB, posicaoA);
        }
    }
}