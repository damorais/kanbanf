﻿using Kanbanf.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kanbanf.Models.Repositorios
{
    public class GruposEmMemoriaRepository
    {
        private static List<Grupo> Grupos = new List<Grupo>();
        private static int ultimoId = 0;

        public void Incluir(string nome, int posicao, Quadro quadro)
        {
            GruposEmMemoriaRepository.ultimoId++;

            var novoGrupo = new Grupo(GruposEmMemoriaRepository.ultimoId, nome, posicao, quadro);

            GruposEmMemoriaRepository.Grupos.Add(novoGrupo);
        }

        public int ObterMaximaPosicaoGrupo(int idQuadro) =>
            Grupos.Select(grupo => grupo.Posicao).DefaultIfEmpty(0).Max();

        public IEnumerable<Grupo> ObterGrupos(int idQuadro) =>
            from grupo in Grupos
            where grupo.Quadro.Id == idQuadro && grupo.Ativo
            orderby grupo.Posicao descending
            select grupo;

        public Grupo Obter(int id) =>
            Grupos.Where(grupo => grupo.GrupoId == id && grupo.Ativo).FirstOrDefault();

        public Grupo ObterGrupoMenorPosicao(int posicao) =>
            (from grupo in Grupos
             where grupo.Posicao < posicao
             orderby grupo.Posicao descending
             select grupo).FirstOrDefault();
        //{
        //    return Grupos.Where(grupo => grupo.Posicao < posicao).OrderByDescending(grupo => grupo.Posicao).FirstOrDefault();
        //}

        public Grupo ObterGrupoMaiorPosicao(int posicao) =>
            (from grupo in Grupos
             where grupo.Posicao > posicao
             orderby grupo.Posicao
             select grupo).FirstOrDefault();

        /// <summary>
        /// Altera informações sobre o quadro
        /// </summary>
        /// <param name="id">Id do quadro a ser modificado</param>
        /// <param name="nome">Novo valor para o nome</param>
        public void Atualizar(int id, string nome)
        {
            var grupo = this.Obter(id);
            var quadro = grupo.Quadro;

            if (grupo != null)
            {
                Grupos.Remove(grupo);
                Grupos.Add(new Grupo(id, nome, grupo.Posicao, quadro));
            }
            else
                throw new ArgumentException($"O elemento com ID {id} não existe");
        }

        public void AtualizarPosicao(Grupo grupo, int novaPosicao)
        {
            Grupos.Remove(grupo);
            Grupos.Add(new Grupo(grupo.GrupoId, grupo.Nome, novaPosicao, grupo.Quadro));
        }

        public void Remover(int id)
        {
            var grupo = this.Obter(id);

            if (grupo != null)
                Grupos.Remove(grupo);
            else
                throw new ArgumentException($"O elemento com ID {id} não existe");
        }
    }
}