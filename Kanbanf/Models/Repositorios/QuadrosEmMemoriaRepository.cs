﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kanbanf.Models.Entidades;

namespace Kanbanf.Models.Repositorios
{
    public class QuadrosEmMemoriaRepository : IQuadrosRepository
    {
        private static List<Quadro> Quadros = new List<Quadro>();
        private static int ultimoId = 0;

        public void Atualizar(int id, string novoNome)
        {
            var quadroASerAlterado = this.Obter(id);

            if (quadroASerAlterado != null)
            {
                Quadros.Remove(quadroASerAlterado);
                Quadros.Add(new Quadro(id, novoNome));
            }
            else
            {
                throw new ArgumentException($"O elemento com ID {id} não existe");
            }
        }

        /// <summary>
        /// Remove um quadro. O status do quadro em questão é alterado de "ativo" para "inativo"
        /// </summary>
        /// <param name="id">O ID do quadro a ser removido / inativado </param>
        public void Remover(int id)
        {
            var quadroARemover = this.Obter(id);

            if (quadroARemover != null)
                quadroARemover.Inativar();
            else
                throw new ArgumentException($"O elemento com ID {id} não existe");
        }

        /// <summary>
        /// Este método remove um quadro permanentemente. Só quadros inativos podem ser removidos
        /// </summary>
        /// <param name="id">O ID do quadro a ser excluído</param>
        public void ExcluirPermanentemente(int id)
        {
            var quadroARemover = this.Obter(id);

            if (quadroARemover != null && !quadroARemover.Ativo)
                Quadros.Remove(quadroARemover);
            else
                throw new ArgumentException($"O elemento com ID {id} não existe ou não está inativo");
        }

        public void Incluir(string nome)
        {
            QuadrosEmMemoriaRepository.ultimoId++;

            var novoQuadro = new Quadro(QuadrosEmMemoriaRepository.ultimoId, nome);

            QuadrosEmMemoriaRepository.Quadros.Add(novoQuadro);
        }

        public Quadro Obter(int id)
        {
            return QuadrosEmMemoriaRepository.Quadros.FirstOrDefault(quadro => quadro.Id == id);
        }

        /// <summary>
        /// Obtém todos os quadros cadastrados
        /// </summary>
        /// <returns>Todos os quadros cadastrados</returns>
        public IEnumerable<Quadro> ObterTodos()
        {
            return QuadrosEmMemoriaRepository.Quadros.OrderBy(quadro => quadro.Nome);
        }

        /// <summary>
        /// Obtém todos os quadros ativos
        /// </summary>
        /// <returns>Todos os quadros ativos</returns>
        public IEnumerable<Quadro> ObterAtivos()
        {
            return QuadrosEmMemoriaRepository.Quadros.Where(quadro => quadro.Ativo).OrderBy(quadro => quadro.Nome); ;
        }

        /// <summary>
        /// Obtém todos os quadros inativos / removidos
        /// </summary>
        /// <returns>Todos os quadros inativos / removidos</returns>
        public IEnumerable<Quadro> ObterInativos()
        {
            return QuadrosEmMemoriaRepository.Quadros.Where(quadro => !quadro.Ativo).OrderBy(quadro => quadro.Nome); ;
        }

        /// <summary>
        /// Reativo um quadro
        /// </summary>
        /// <param name="id"></param>
        public void Restaurar(int id)
        {
            var quadroARestaurar = this.Obter(id);

            if (quadroARestaurar != null)
                quadroARestaurar.Restaurar();
            else
                throw new ArgumentException($"O elemento com ID {id} não existe");
        }
    }
}