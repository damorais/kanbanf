﻿using Kanbanf.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kanbanf.Models.Repositorios
{
    interface IQuadrosRepository
    {
        Quadro Obter(int id);

        IEnumerable<Quadro> ObterTodos();
        IEnumerable<Quadro> ObterAtivos();
        IEnumerable<Quadro> ObterInativos();

        void Incluir(string nome);
        void Atualizar(int id, string novoNome);
        void Remover(int id);
        void ExcluirPermanentemente(int id);
        void Restaurar(int id);
    }
}
