﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kanbanf.Models.Entidades
{
    public class Grupo
    {
        public int GrupoId { get; private set; }
        public string Nome { get; private set; }
        public DateTime DataDeCriacao { get; }
        public DateTime DataDeAlteracao { get; private set; }
        public bool Ativo { get; private set; }
        public Quadro Quadro { get; private set; }
        public int Posicao { get; private set; }

        public Grupo()
        {
            this.DataDeCriacao = DateTime.Now;
            this.DataDeAlteracao = DateTime.Now;
        }

        public Grupo(int id, string nome, int posicao, Quadro quadro) : this()
        {
            this.GrupoId = id;
            this.Nome = nome;
            this.Quadro = quadro;
            this.Ativo = true;
            this.Posicao = posicao;
        }

        public void Inativar()
        {
            this.Ativo = false;
            this.DataDeAlteracao = DateTime.Now;
        }
    }
}