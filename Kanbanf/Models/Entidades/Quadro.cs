﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Kanbanf.Models.Entidades
{
    public class Quadro
    {
        public int Id { get; private set; }
        public string Nome { get; private set; }
        public bool Ativo { get; private set; }

        [DisplayName("Data de Criação")]
        public DateTime DataDeCriacao { get; private set; }

        [DisplayName("Data de Alteração")]
        public DateTime DataDeAlteracao { get; private set; }

        public Quadro(int id, string nome)
        {
            this.Id = id;
            this.Nome = nome;
            this.DataDeCriacao = DateTime.Now;
            this.DataDeAlteracao = DateTime.Now;
            this.Ativo = true;
        }

        public void Restaurar()
        {
            //Altero o nome do quadro, para evitar que exista dois quadros com o mesmo nome. Para isso, 
            //adiciono a data e hora da restauração
            this.Nome = $"{this.Nome} (Restaurado em {DateTime.Now.ToString("dd-mm-yyyy HH:mm:ss")})";
            this.Ativo = true;
            this.DataDeAlteracao = DateTime.Now;
        }

        public void Inativar()
        {
            this.Ativo = false;
            this.DataDeAlteracao = DateTime.Now;
        }

        public override bool Equals(object obj)
        {
            var quadro = obj as Quadro;
            return quadro != null &&
                   Id == quadro.Id &&
                   Nome == quadro.Nome;
        }

        public override int GetHashCode()
        {
            var hashCode = -1643562096;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nome);
            return hashCode;
        }
    }
}