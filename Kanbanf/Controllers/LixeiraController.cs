﻿using Kanbanf.Models.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kanbanf.Controllers
{
    public class LixeiraController : Controller
    {
        public ActionResult List()
        {
            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();

            var quadros = repository.ObterInativos();

            return View(quadros);
        }

        public ActionResult Delete(int id)
        {
            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();
            repository.ExcluirPermanentemente(id);

            //TODO: Adicionar mensagem de alerta informando o sucesso da operação
            return RedirectToAction("list");
        }

        public ActionResult Restore(int id)
        {
            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();
            repository.Restaurar(id);

            //TODO: Adicionar mensagem de alerta informando o sucesso da operação
            return RedirectToAction("list", "quadros");
        }
    }
}