﻿using Kanbanf.Models.Entidades;
using Kanbanf.Models.Repositorios;
using System.Web.Mvc;

namespace Kanbanf.Controllers
{
    public class QuadrosController : Controller
    {
        public ActionResult List()
        {
            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();

            var quadros = repository.ObterAtivos();

            return View(quadros);
        }

        public ActionResult Delete(int id)
        {
            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();
            repository.Remover(id);

            return RedirectToAction("list");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection formulario)
        {
            var nomeQuadro = formulario["txtNomeQuadro"];

            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();
            repository.Incluir(nomeQuadro);

            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();

            var quadro = repository.Obter(id);

            if (quadro != null)
                return View(quadro);
            else
                return HttpNotFound("Quadro não encontrado");
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection formulario)
        {
            var novoNome = formulario["txtNomeQuadro"];

            IQuadrosRepository repository = new QuadrosEmMemoriaRepository();
            repository.Atualizar(id, novoNome);

            return RedirectToAction("List");
        }
    }
}